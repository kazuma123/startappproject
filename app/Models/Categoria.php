<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    //Establecer relacion muchos a muchos a
    public function startup(){
        return $this->belongsToMany('App\Models\StartUp');
    }

    protected $table = 'categorias';
    protected $fillable = [
        'nombre_categoria',
        'descripcion'

    ];
}
