<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EtapaLevantamiento extends Model
{
    use HasFactory;
    public function startup(){
        return $this->hasMany('App\Models\StartUp');
    }
}
