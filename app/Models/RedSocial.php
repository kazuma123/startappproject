<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RedSocial extends Model
{
    use HasFactory;
    
    protected $table = 'red_socials';
    protected $fillable = [
        'nombre',
        'descripcion'

    ];
    
    public function startup(){
        return $this->belongsToMany('App\Models\StartUp');
    }
}
