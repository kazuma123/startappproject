<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StartUp extends Model
{
    use HasFactory;

    public function categorias(){
        return $this->belongsToMany('App\Models\Categoria');
    }
    public function red_social(){
        return $this->belongsToMany('App\Models\RedSocial');
    }
    public function direccion(){
        return $this->belongsToMany('App\Models\Direccion');
    }

    // Relacion uno a muchos

    public function Localizacion(){
        return $this->hasMany('App\Models\Localizacion');
    }
    public function TipoFinanciacion(){
        return $this->hasMany('App\Models\TipoFinanciacion');
    }
    public function EtapaLevantamiento(){
        return $this->hasMany('App\Models\EtapaLevantamiento');
    }
    public function FaseNegocio(){
        return $this->hasMany('App\Models\FaseNegocio');
    }
}

