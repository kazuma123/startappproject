<?php

namespace Database\Factories;

use App\Models\StartUp;
use Illuminate\Database\Eloquent\Factories\Factory;

class StartUpFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StartUp::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
