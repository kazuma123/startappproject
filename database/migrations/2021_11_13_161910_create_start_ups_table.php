<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStartUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_ups', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_startup');
            $table->foreignId('id_localizacion')->constrained('localizacions')->onDelete('set null');
            $table->foreignId('id_tipo_financiacion')->constrained('tipo_financiacions')->onDelete('set null');;
            $table->foreignId('id_etapa_levantamiento')->constrained('etapa_levantamientos')->onDelete('set null');
            $table->foreignId('id_tipo_fase_negocio')->constrained('fase_negocios')->onDelete('set null');
            $table->string('descripcion');
            $table->string('public_objetivo');

         

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_ups');
    }
}
