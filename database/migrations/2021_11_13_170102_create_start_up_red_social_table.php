<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStartUpRedSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_up_red_social', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_startup');
            $table->unsignedBigInteger('id_red_social');
            
            $table->foreign('id_startup')->references('id')->on('start_ups')->onDelete('cascade');
            $table->foreign('id_red_social')->references('id')->on('red_socials')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_up_red_social');
    }
}
