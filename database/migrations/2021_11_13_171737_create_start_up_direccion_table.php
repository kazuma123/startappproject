<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStartUpDireccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_up_direccion', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('id_startup');
            $table->unsignedBigInteger('id_direccion');
            
            $table->foreign('id_startup')->references('id')->on('start_ups')->onDelete('cascade');
            $table->foreign('id_direccion')->references('id')->on('direccions')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_up_direccion');
    }
}
