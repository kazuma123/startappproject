<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\TipoUsuarioController;
use App\Http\Controllers\RedSocialController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
Route::get('/products', [ProductController::class, 'index']);
Route::get('products/{id}', [ProductController::class, 'show']);
Route::get('products/search/{name}', [ProductController::class, 'search']);

// rutas categorias
Route::get('/categoria', [CategoriaController::class, 'index']);
Route::get('categoria/{id}', [CategoriaController::class, 'show']);
Route::post('/categoria', [CategoriaController::class, 'store']);
Route::put('/categoria/{id}', [CategoriaController::class, 'update']);
Route::delete('/categoria/{id}', [CategoriaController::class, 'destroy']);
Route::get('categoria/search/{name}', [CategoriaController::class, 'search']);

// rutas redsocial
Route::get('/redsocial', [RedSocialController::class, 'index']);
Route::get('redsocial/{id}', [RedSocialController::class, 'show']);
Route::post('/redsocial', [RedSocialController::class, 'store']);
Route::put('/redsocial/{id}', [RedSocialController::class, 'update']);
Route::delete('/redsocial/{id}', [RedSocialController::class, 'destroy']);
Route::get('redsocial/search/{name}', [RedSocialController::class, 'search']);

// rutas redsocial
Route::get('/tipousuario', [TipoUsuarioController::class, 'index']);
Route::get('tipousuario/{id}', [TipoUsuarioController::class, 'show']);
Route::post('/tipousuario', [TipoUsuarioController::class, 'store']);
Route::put('/tipousuario/{id}', [TipoUsuarioController::class, 'update']);
Route::delete('/tipousuario/{id}', [TipoUsuarioController::class, 'destroy']);
Route::get('tipousuario/search/{name}', [TipoUsuarioController::class, 'search']);


//protected routes
Route::group(['middleware' => ['auth:sanctum']], function () {

    //route users
    Route::post('/products', [ProductController::class, 'store']);
    Route::put('/products/{id}', [ProductController::class, 'update']);
    Route::delete('/products/{id}', [ProductController::class, 'destroy']);

    

    //logout
    Route::post('/logout', [AuthController::class, 'logout']);
    
});


//Route::get('/products', [ProductController::class, 'index']);
//Route::post('/products', [ProductController::class, 'store']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
